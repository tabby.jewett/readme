# Howdy!  


## A little about me 
- My name is Tabitha Jewett, but I go by Tabby (feels less grandmother-y) 
- I currently live in the grungy but beautiful surfer town of Ventura, CA with my husband Skylar, our 6 month boy, Parker, and our golden retriever, Tucker
- Though I live in California, I’m a midwestern at heart -- raised in a small town of 1,200 people called Louisville, Nebraska
- I’m an [introverted extrovert](https://en.wikipedia.org/wiki/Extraversion_and_introversion) which for me means that I enjoy spending time with others but I recharge in solitude. 
- I’m notorious for my spelling errors and we can make fun of it together 😊
- I have strong opinions, but they’re weakly held. I love finding a new perspective that makes me change my opinion, because it means that I’ve gotten a chance to learn something substantive and new.
- I strongly believe in [Patrick Lencioni’s pyramid hierarchy](https://www.hecouncil.org/clientuploads/directory/organizational_effectiveness/5%20behaviors/The-Five-Behaviors-Model-Pyramid-Graphic---Full-Descriptions.jpg) of cohesive, high-performing team and try to model that framework with my teams.  
- I’m extremely passionate about anything I care about -- that can be both a strength and a weakness 
- I prefer to discuss things directly, both with my teams and my manager. I believe in [radical candor](https://www.radicalcandor.com/our-approach/). 
- I believe in playing to your strengths, and building complete teams based on coordinated strengths.
- I aspire to use inclusive language and actively listen. Please call out when I make mistakes or fail. I will ALWAYS thank you for it.

## How I work 
- My timezone is [PST](https://time.is/Ventura) (Pacific Standard Time)
- My work hours are 8AM PST (10AM CT) to 4PM PST (6PM CT) with a 1/2 hour lunch break at 12PM (2PM CT) 
- Before 8AM (10AM CT) and after 4PM (6PM CT), I’m on kid duty
- If I do need to wrap up work it will be after 8:00PM (10PM CT) (once the kiddo is down for bed) 
- I like prioritize deep work in the mornings 

## A typical day 
- I get up at 6AM PST on most days (if my kiddo doesn’t wake me up first)
- I start working at 8AM PST and take my first break around 12PM PST
- I check my email in the morning around 8AM and after lunch at 1PM
- If I don't respond to slack's in the moment, I respond the team at the end of the day
- I workout 3-4 times per week and try to do this between 4-5PM

## What I am currently focused on 
- Currently, soaking up as much knowledge and information as possible. I would love help understanding the industry, customers, user journey, jobs to be done, competitors, 5-10x growth opportunities, your product area, etc. 
- Getting to know as many people as I can -- priotizing key team members first and then expanding out more 
- Partnering with the team to design and plan out the future of our products 

## How to reach me 
- **Slack:** Mentioning me is the best way to reach me during the day. I will always have Slack open during business hours and typically will respond to @ as soon as I see it. If I don't respond right away I'm either busy or unable to message at that time -- but I prioritize responding to all message by EOD. Sometimes I will set my self to away when I am in deep work. Expect my responses on email and Slack to be asynchronous in this state.
- **E-mail:** I read my emails a few times a day, if you need something immediately or I am not responding quickly enough, please reach out on Slack.

## Personality test 
- Myers Briggs - [INFJ](https://www.16personalities.com/infj-personality)
- Strengths Finder (Achiever, Harmoney, Relator, Developer, Arranger) 
